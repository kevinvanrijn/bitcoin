import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # or any {'0', '1', '2'}
import numpy as np
import tensorflow as tf

config = tf.compat.v1.ConfigProto(gpu_options=
								  tf.compat.v1.GPUOptions(per_process_gpu_memory_fraction=0.8)
								  # device_count = {'GPU': 1}
								  )
config.gpu_options.allow_growth = True
session = tf.compat.v1.Session(config=config)
tf.compat.v1.keras.backend.set_session(session)
from tensorflow import keras
import pandas as pd
import seaborn as sns
from pylab import rcParams
import matplotlib.pyplot as plt
from matplotlib import rc
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.layers import Bidirectional, Dropout, Activation, Dense, LSTM
from tensorflow.python.keras.layers import CuDNNLSTM
from tensorflow.keras.models import Sequential

# CSV Files
# bitcoin
# USD
csv_path = "https://query1.finance.yahoo.com/v7/finance/download/BTC-USD?period1=1410912000&period2=1609718400&interval=1d&events=history&includeAdjustedClose=true"
# EUR
# csv_path = "https://query1.finance.yahoo.com/v7/finance/download/BTC-EUR?period1=1410912000&period2=1609718400&interval=1d&events=history&includeAdjustedClose=true"

# AMD
# csv_path = "https://query1.finance.yahoo.com/v7/finance/download/AMD?period1=322099200&period2=1609545600&interval=1d&events=history&includeAdjustedClose=true"


# Seaborn
sns.set(style='whitegrid', palette='muted', font_scale=1.5)

rcParams['figure.figsize'] = 14, 8

RANDOM_SEED = 42

np.random.seed(RANDOM_SEED)

# Get data
df = pd.read_csv(csv_path, parse_dates=['Date'])
df = df.sort_values('Date')
# print(df.head)

# Plot closing price
# ax = df.plot(x='Date', y='Close')
# ax.set_xlabel('Date')
# ax.set_ylabel('Close Price USD')
# plt.show()

scaler = MinMaxScaler()
close_price = df.Close.values.reshape(-1, 1)
scaled_close = scaler.fit_transform(close_price)
# print("close_price: Type=:", type(close_price), ", len=", len(close_price), ", type in array=", type(close_price[0]), ", type in array in array=", type(close_price[0][0]))
# print("scaled_price: Type=:", type(scaled_close), ", len=", len(scaled_close), ", type in array=", type(scaled_close[0]), ", type in array in array=", type(scaled_close[0][0]))
# print("Execute isnan on scaled")

np.isnan(scaled_close).any()
scaled_close = scaled_close[~np.isnan(scaled_close)]
scaled_close = scaled_close.reshape(-1, 1)
# print("\nAfter reshape and isnan\nscaled_price: Type=:", type(scaled_close), ", len=", len(scaled_close), ", type in array=", type(scaled_close[0]), ", type in array in array=", type(scaled_close[0][0]))


# print(type(close_price[0][0]))
# print('\n\n')

import random
lst = []
for i in range(9):
	lst.append([random.randrange(29000.0, 33000.0)])
ua = np.array(lst, np.float64)

print(ua, ua.shape)
a = scaler.fit_transform(ua)
print(a)
print(scaler.inverse_transform(a))


# Preprocessing
SEQ_LEN = 10


def to_sequences(data, seq_len):
	d = []

	for index in range(len(data) - seq_len):
		d.append(data[index: index + seq_len])

	return np.array(d)


def preprocess(data_raw, seq_len, train_split):
	data = to_sequences(data_raw, seq_len)

	num_train = int(train_split * data.shape[0])

	X_train = data[:num_train, :-1, :]
	y_train = data[:num_train, -1, :]

	X_test = data[num_train:, :-1, :]
	y_test = data[num_train:, -1, :]

	return X_train, y_train, X_test, y_test


print('\n\n')
X_train, y_train, X_test, y_test = preprocess(scaled_close, SEQ_LEN, train_split=0.95)
print("Total", scaled_close.shape)
print("Train size", X_train.shape)
print("Test size", X_test.shape)
print('\n\n')

# exit()


# Model
DROPOUT = 0.2
WINDOW_SIZE = SEQ_LEN - 1

model = keras.Sequential()

model.add(Bidirectional(CuDNNLSTM(WINDOW_SIZE, return_sequences=True),
						input_shape=(WINDOW_SIZE, X_train.shape[-1])))
model.add(Dropout(rate=DROPOUT))

model.add(Bidirectional(CuDNNLSTM((WINDOW_SIZE * 2), return_sequences=True)))
model.add(Dropout(rate=DROPOUT))

model.add(Bidirectional(CuDNNLSTM(WINDOW_SIZE, return_sequences=False)))

model.add(Dense(units=1))

model.add(Activation('linear'))

# Training
model.compile(
	loss='mean_squared_error',
	optimizer='adam'
)

BATCH_SIZE = 64

history = model.fit(
	X_train,
	y_train,
	epochs=10,
	batch_size=BATCH_SIZE,
	shuffle=False,
	validation_split=0.1
)

model.evaluate(X_test, y_test)

# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.show()


# Prediction
y_hat = model.predict(X_test)

y_test_inverse = scaler.inverse_transform(y_test)
y_hat_inverse = scaler.inverse_transform(y_hat)

plt.plot(y_test_inverse, label="Actual Price", color='green')
plt.plot(y_hat_inverse, label="Predicted Price", color='red')

plt.title('bitcoin price prediction')
plt.xlabel('Time [days]')
plt.ylabel('Price')
plt.legend(loc='best')

plt.show()

model.save('btc.h5')
print('Saved Model')
print(model.predict(a))

