from datetime import timedelta, date, datetime
from dateutil.relativedelta import relativedelta

url = "https://query1.finance.yahoo.com/v7/finance/download/{ticker}?period1={period1}&period2={period2}&interval=1d&events=history&includeAdjustedClose=true"


def get_updated_csv(dateA, ticker='BTC-USD', **kwargs):
	"""
	:param start_date: The start date for the csv (can be str, datetime or date) (YYY-mm-dd)
	:param ticker: Ticker for which to get csv
	:param years: Amount of years to get data up until today
	:return: Url for csv.
	"""

	if type(dateA) == str:
		# print("Is String")
		pass
	elif type(dateA) == date:
		# print("Is Date")
		start_date = str(dateA)
	elif type(dateA) == datetime:
		# print("Is Datetime")
		dateA = datetime.strftime(dateA, "%Y-%m-%d")

	start_date = date.strftime(datetime.strptime(dateA, "%Y-%m-%d") - relativedelta(**kwargs), "%Y-%m-%d")
	begin_date = datetime.strptime(start_date, "%Y-%m-%d") + timedelta(days=1)
	end_date = datetime.today()

	csv_path = url.format(ticker=ticker, period1=round(begin_date.timestamp()), period2=round(end_date.timestamp()))
	return csv_path


dateA = date.today()
print(get_updated_csv(dateA, years=1))
print(get_updated_csv(dateA, years=2, days=5))
print(get_updated_csv(dateA, years=3, weeks=16, days=10))


def test(*args, **kwargs):
	print(args)
	print(kwargs)


test('a', 'b', date=dateA)
